#include <iostream>
#include "person.pb.h"

int main() {
	proto::Person person;
	person.set_name("John");
	std::cout << "Hello, " << person.name() << std::endl;
	return 0;
}
