# issue-bazel-protobuf-compile

## Issue

- Bazel compiles protobuf from sources which takes 15 min (more then the project itself)

## Links

- Stackoverflow [Is it possible to use Bazel without compiling protobuf compiler?](https://stackoverflow.com/questions/68918369/is-it-possible-to-use-bazel-without-compiling-protobuf-compiler)
- Groups [How to avoid compiling protobuf compiler and use precompiled protoc?](https://groups.google.com/u/1/g/bazel-discuss/c/3Q_GEqNZrC0)
- Groups [is avoiding protobuf compilation a false target?](https://groups.google.com/u/1/g/bazel-discuss/c/6vRCtiExiOM)

## Folder Structure

### v1

Build protobuf from sources.

Pros:
- the most fresh version of protobuf
- easy to install

Cons:
- waste of time building proto compiler (when it's already available on the system)
- for small projects most time spent on building proto compiler

Completed successfully in about 8 min:
[log](https://gitlab.com/mvfwd/issue-bazel-protobuf-compile/-/jobs/1564845791)

Some of the previous runs (15 min)
![screenshot](./screenshot_build_time.png)

### v2

Use pre-installed protoc compiler to build proto files, then build project itself.
Running `./run_protoc.sh` creates `proto-bin` folder with `person.pb.cc`, `person.pb.h` and `BUILD` files. 

Pros:
- fast compilation of proto files (no time waste on building proto compiler)

Cons:
- build all now requires two steps
  1) build proto files
  2) use bazel to build source files

Failure ([log](https://gitlab.com/mvfwd/issue-bazel-protobuf-compile/-/jobs/1564845794))
```bash
ERROR: /builds/mvfwd/issue-bazel-protobuf-compile/v2/proto-bin/BUILD:4:11: Compiling proto-bin/person.pb.cc failed: (Exit 1): gcc failed: error executing command /usr/bin/gcc -U_FORTIFY_SOURCE -fstack-protector -Wall -Wunused-but-set-parameter -Wno-free-nonheap-object -fno-omit-frame-pointer '-std=c++0x' -MD -MF ... (remaining 16 argument(s) skipped)
Use --sandbox_debug to see verbose messages from the sandbox
In file included from proto-bin/person.pb.cc:4:
proto-bin/person.pb.h:10:10: fatal error: google/protobuf/port_def.inc: No such file or directory
   10 | #include <google/protobuf/port_def.inc>
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
compilation terminated.
INFO: Elapsed time: 12.021s, Critical Path: 0.13s
INFO: 2 processes: 2 internal.
FAILED: Build did NOT complete successfully
FAILED: Build did NOT complete successfully
```

### v3

Set up Bazel to use pre-installed protoc compiler

Pros:
- no time waste on building proto compiler
- build all with one command `bazel build ...`

Cons:
- tricky to set up

Failure ([log](https://gitlab.com/mvfwd/issue-bazel-protobuf-compile/-/jobs/1564845795)):

```bash
ERROR: /builds/mvfwd/issue-bazel-protobuf-compile/v3/BUILD:14:10: Compiling main.cc failed: (Exit 1): gcc failed: error executing command /usr/bin/gcc -U_FORTIFY_SOURCE -fstack-protector -Wall -Wunused-but-set-parameter -Wno-free-nonheap-object -fno-omit-frame-pointer '-std=c++0x' -MD -MF bazel-out/k8-fastbuild/bin/_objs/main/main.pic.d ... (remaining 19 argument(s) skipped)
Use --sandbox_debug to see verbose messages from the sandbox
In file included from main.cc:2:
bazel-out/k8-fastbuild/bin/person.pb.h:10:10: fatal error: google/protobuf/port_def.inc: No such file or directory
   10 | #include <google/protobuf/port_def.inc>
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
compilation terminated.
INFO: Elapsed time: 15.373s, Critical Path: 0.21s
INFO: 13 processes: 11 internal, 2 linux-sandbox.
FAILED: Build did NOT complete successfully
FAILED: Build did NOT complete successfully
```

After building protobuf from sources it still fails with 
https://gitlab.com/mvfwd/issue-bazel-protobuf-compile/-/jobs/1570396207

```bash
[13 / 15] [Scann] Compiling person.pb.cc
ERROR: /builds/mvfwd/issue-bazel-protobuf-compile/v3/BUILD:14:10: Linking main failed: (Exit 1): gcc failed: error executing command /usr/bin/gcc @bazel-out/k8-fastbuild/bin/main-2.params
Use --sandbox_debug to see verbose messages from the sandbox
bazel-out/k8-fastbuild/bin/_objs/main/main.pic.o:main.cc:function void google::protobuf::internal::ArenaStringPtr::Set<google::protobuf::internal::ArenaStringPtr::EmptyDefault>(google::protobuf::internal::ArenaStringPtr::EmptyDefault, char const*, google::protobuf::Arena*): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Set(google::protobuf::internal::ArenaStringPtr::EmptyDefault, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::Person(proto::Person const&): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Set(google::protobuf::internal::ArenaStringPtr::EmptyDefault, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::Clear(): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::ClearToEmpty()'
...
```
