# issue-bazel-protobuf-compile

## Issue

https://stackoverflow.com/questions/68918369/is-it-possible-to-use-bazel-without-compiling-protobuf-compiler

Is it possible to use Bazel without compiling protobuf compiler?

I have some projects using Bazel, C++ and protobuf. I also use gitlab CI/CD to build, test, check coverage, etc. The problem is that when the project compiles first time it also compiles a protobuf compiler, which adds about 15 minutes to each step (the step itself takes 1-5 min).

I was using a setup example for this documentation: 
https://blog.bazel.build/2017/02/27/protocol-buffers.html

Here I created a simple hello world example with protobuf.
When I use `protoc` to generate `*.pb.cc`, `*.pb.h` files it takes about 5 seconds.
When I use `bazel build ...` it takes 15 minutes, because it builds protobuf compiler.
Build log: https://gitlab.com/mvfwd/issue-bazel-protobuf-compile/-/jobs/1532045913

Is there any other way to setup Bazel to use already precompiled `protoc` and to skip 15 min on every step?

![screenshot](./screenshot_build_time.png)

### Bind proto_compiler

After using [this](https://github.com/google/startup-os/blob/master/WORKSPACE#L173) as example, binded "proto_compiler" and was able to build `:person_proto`

```bash
$ bazel build :person_proto
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
INFO: Analyzed target //:person_proto (0 packages loaded, 0 targets configured).
INFO: Found 1 target...
Target //:person_proto up-to-date:
  bazel-bin/person_proto-descriptor-set.proto.bin
INFO: Elapsed time: 0.065s, Critical Path: 0.00s
INFO: 1 process: 1 internal.
INFO: Build completed successfully, 1 total action
```

But building next step fails:

```bash
$ bazel build :person_cc_proto
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
INFO: Build option --proto_toolchain_for_cc has changed, discarding analysis cache.
ERROR: /home/path/issue-bazel-protobuf-compile/BUILD:2:14: every rule of type proto_library implicitly depends upon the target '@com_google_protobuf//:cc_toolchain', but this target could not be found because of: no such package '@com_google_protobuf//': The repository '@com_google_protobuf' could not be resolved
ERROR: Analysis of target '//:person_cc_proto' failed; build aborted: Evaluation of aspect BazelCcProtoAspect on //:person_proto failed: com.google.devtools.build.lib.packages.BuildFileNotFoundException: no such package '@com_google_protobuf//': The repository '@com_google_protobuf' could not be resolved
INFO: Elapsed time: 0.158s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (0 packages loaded, 74 targets configured)
```

## Time

Locally:
```bash
$ time ./run_bazel_build.sh 
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
INFO: Analyzed 3 targets (18 packages loaded, 572 targets configured).
INFO: Found 3 targets...
INFO: Elapsed time: 183.326s, Critical Path: 21.99s
INFO: 293 processes: 9 internal, 284 linux-sandbox.
INFO: Build completed successfully, 293 total actions

real	3m4.777s
user	0m0.528s
sys	0m0.143s
```

## Debug

When empty WORKSPACE
```bash
$ ./run_bazel_build.sh 
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
ERROR: /home/path/issue-bazel-protobuf-compile/BUILD:2:14: every rule of type proto_library implicitly depends upon the target '@com_google_protobuf//:protoc', but this target could not be found because of: no such package '@com_google_protobuf//': The repository '@com_google_protobuf' could not be resolved
ERROR: Analysis of target '//:person_proto' failed; build aborted: Analysis failed
INFO: Elapsed time: 0.269s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (0 packages loaded, 0 targets configured)
```

## Steps

1. Added rules_cc, rules_proto. Failure

```bash
$ bazel build ...
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
ERROR: /home/m/.cache/bazel/_bazel_m/f0141ef11d13e535ea9f85829b33e133/external/com_google_protobuf/BUILD:1006:21: in proto_lang_toolchain rule @com_google_protobuf//:cc_toolchain: '@com_google_protobuf//:cc_toolchain' does not have mandatory provider 'ProtoInfo'.
ERROR: Analysis of target '//:main' failed; build aborted: Analysis of target '@com_google_protobuf//:cc_toolchain' failed
INFO: Elapsed time: 0.263s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (2 packages loaded, 8 targets configured)
```

1. Update

From
```starlark
http_archive(
    name = "rules_cc",
    sha256 = "35f2fb4ea0b3e61ad64a369de284e4fbbdcdba71836a5555abb5e194cf119509",
    strip_prefix = "rules_cc-624b5d59dfb45672d4239422fa1e3de1822ee110",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_cc/archive/624b5d59dfb45672d4239422fa1e3de1822ee110.tar.gz",
        "https://github.com/bazelbuild/rules_cc/archive/624b5d59dfb45672d4239422fa1e3de1822ee110.tar.gz",
    ],
)
```

To Aug 2021, https://github.com/bazelbuild/rules_cc/commit/d66a13e2a01630afcafc4ba411d83e291ecf02bd
```starlark
http_archive(
    name = "rules_cc",
    sha256 = "3cde212ccda3ba152897e7fd354c42eba275878b6d98fe4f2125c684a73f3842",
    strip_prefix = "rules_cc-d66a13e2a01630afcafc4ba411d83e291ecf02bd",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_cc/archive/d66a13e2a01630afcafc4ba411d83e291ecf02bd.tar.gz",
        "https://github.com/bazelbuild/rules_cc/archive/d66a13e2a01630afcafc4ba411d83e291ecf02bd.tar.gz",
    ],
)
```

```bash
shasum -a 256 file.tar.gz
```

Still same failure.

From Protobuf to v3.11.3, Mar 2, 2020, https://github.com/bazelbuild/rules_proto/pull/49
```starlark
http_archive(
    name = "rules_proto",
    sha256 = "2490dca4f249b8a9a3ab07bd1ba6eca085aaf8e45a734af92aad0c42d9dc7aaf",
    strip_prefix = "rules_proto-218ffa7dfa5408492dc86c01ee637614f8695c45",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/218ffa7dfa5408492dc86c01ee637614f8695c45.tar.gz",
        "https://github.com/bazelbuild/rules_proto/archive/218ffa7dfa5408492dc86c01ee637614f8695c45.tar.gz",
    ],
)
```

To Aug, 2021, https://github.com/bazelbuild/rules_proto/commit/fcad4680fee127dbd8344e6a961a28eef5820ef4
```starlark
http_archive(
    name = "rules_proto",
    sha256 = "36476f17a78a4c495b9a9e70bd92d182e6e78db476d90c74bac1f5f19f0d6d04",
    strip_prefix = "rules_proto-fcad4680fee127dbd8344e6a961a28eef5820ef4",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/fcad4680fee127dbd8344e6a961a28eef5820ef4.tar.gz",
        "https://github.com/bazelbuild/rules_proto/archive/fcad4680fee127dbd8344e6a961a28eef5820ef4.tar.gz",
    ],
)
```

Different error

```bash
$ bazel build ...
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
WARNING: Download from https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/fcad4680fee127dbd8344e6a961a28eef5820ef4.tar.gz failed: class com.google.devtools.build.lib.bazel.repository.downloader.UnrecoverableHttpException GET returned 404 Not Found
ERROR: /home/m/.cache/bazel/_bazel_m/f0141ef11d13e535ea9f85829b33e133/external/com_google_protobuf/BUILD:84:10: error loading package '@com_github_protocolbuffers_protobuf//': cannot load '@bazel_skylib//rules:common_settings.bzl': no such file and referenced by '@com_google_protobuf//:cc_toolchain'
ERROR: Analysis of target '//:main' failed; build aborted: Analysis failed
INFO: Elapsed time: 3.565s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (21 packages loaded, 90 targets configured)
```

Fix typo in protoc_bin, add sha256, different error

```bash
$ bazel build ...
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
ERROR: /home/m/.cache/bazel/_bazel_m/f0141ef11d13e535ea9f85829b33e133/external/com_google_protobuf/BUILD:84:10: error loading package '@com_github_protocolbuffers_protobuf//': cannot load '@bazel_skylib//rules:common_settings.bzl': no such file and referenced by '@com_google_protobuf//:cc_toolchain'
ERROR: Analysis of target '//:main' failed; build aborted: Analysis failed
INFO: Elapsed time: 1.124s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (21 packages loaded, 90 targets configured)
```

Overriden --proto_toolchain_for_cc //external:cc_toolchain

```bash
$ bazel build ...
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
ERROR: /home/m/Synology/drive/prog/2021/b/issue-bazel-protobuf-compile/tools/BUILD:11:21: in runtime attribute of proto_lang_toolchain rule //tools:cc_toolchain: rule '//tools:protobuf' does not exist
ERROR: Analysis of target '//tools:cc_toolchain' failed; build aborted: Analysis of target '//tools:cc_toolchain' failed
INFO: Elapsed time: 0.159s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (4 packages loaded, 5 targets configured)
```

Removed cc_toolchain runtime

```bash
$ bazel build ...
WARNING: Ignoring JAVA_HOME, because it must point to a JDK, not a JRE.
ERROR: /home/m/Synology/drive/prog/2021/b/issue-bazel-protobuf-compile/BUILD:2:14: in :aspect_cc_proto_toolchain attribute of BazelCcProtoAspect aspect on proto_library rule //:person_proto: '@local_config_cc//:toolchain' does not have mandatory providers: ProtoLangToolchainProvider
ERROR: Analysis of target '//:main' failed; build aborted: Analysis of target '//:person_proto' failed
INFO: Elapsed time: 0.139s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (1 packages loaded, 5 targets configured)
```

After changing `//external:cc_toolchain` to `//tools:cc_toolchain` in `.bazelrc` proto started to compile:

```bash
$ bazel build :person_cc_proto
INFO: Analyzed target //:person_cc_proto (0 packages loaded, 0 targets configured).
INFO: Found 1 target...
Target //:person_cc_proto up-to-date:
  bazel-bin/person.pb.h
  bazel-bin/person.pb.cc
  bazel-bin/libperson_proto.a
  bazel-bin/libperson_proto.so
INFO: Elapsed time: 0.110s, Critical Path: 0.00s
INFO: 1 process: 1 internal.
INFO: Build completed successfully, 1 total action
```

But building `main` fails:

```bash
$ bazel build :main
INFO: Analyzed target //:main (0 packages loaded, 0 targets configured).
INFO: Found 1 target...
ERROR: /home/m/Synology/drive/prog/2021/b/issue-bazel-protobuf-compile/BUILD:14:10: Linking main failed: (Exit 1): gcc failed: error executing command /usr/bin/gcc @bazel-out/k8-fastbuild/bin/main-2.params

Use --sandbox_debug to see verbose messages from the sandbox
bazel-out/k8-fastbuild/bin/_objs/main/main.pic.o:main.cc:function void google::protobuf::internal::ArenaStringPtr::Set<google::protobuf::internal::ArenaStringPtr::EmptyDefault>(google::protobuf::internal::ArenaStringPtr::EmptyDefault, char const*, google::protobuf::Arena*): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Set(google::protobuf::internal::ArenaStringPtr::EmptyDefault, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::Person(proto::Person const&): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Set(google::protobuf::internal::ArenaStringPtr::EmptyDefault, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::Clear(): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::ClearToEmpty()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_InternalParse(char const*, google::protobuf::internal::ParseContext*): error: undefined reference to 'google::protobuf::internal::InlineGreedyStringParser(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >*, char const*, google::protobuf::internal::ParseContext*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_InternalParse(char const*, google::protobuf::internal::ParseContext*): error: undefined reference to 'google::protobuf::internal::UnknownFieldParse(unsigned long, google::protobuf::UnknownFieldSet*, char const*, google::protobuf::internal::ParseContext*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_InternalSerialize(unsigned char*, google::protobuf::io::EpsCopyOutputStream*) const: error: undefined reference to 'google::protobuf::internal::WireFormatLite::VerifyUtf8String(char const*, int, google::protobuf::internal::WireFormatLite::Operation, char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_InternalSerialize(unsigned char*, google::protobuf::io::EpsCopyOutputStream*) const: error: undefined reference to 'google::protobuf::UnknownFieldSet::default_instance()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_InternalSerialize(unsigned char*, google::protobuf::io::EpsCopyOutputStream*) const: error: undefined reference to 'google::protobuf::internal::WireFormat::InternalSerializeUnknownFieldsToArray(google::protobuf::UnknownFieldSet const&, unsigned char*, google::protobuf::io::EpsCopyOutputStream*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::ByteSizeLong() const: error: undefined reference to 'google::protobuf::internal::ComputeUnknownFieldsSize(google::protobuf::internal::InternalMetadata const&, unsigned long, google::protobuf::internal::CachedSize*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::LogMessage::LogMessage(google::protobuf::LogLevel, char const*, int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::LogMessage::operator<<(char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::LogFinisher::operator=(google::protobuf::internal::LogMessage&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::LogMessage::~LogMessage()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Set(google::protobuf::internal::ArenaStringPtr::EmptyDefault, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::MergeFrom(proto::Person const&): error: undefined reference to 'google::protobuf::internal::LogMessage::~LogMessage()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::GetMetadata() const: error: undefined reference to 'google::protobuf::internal::AssignDescriptors(google::protobuf::internal::DescriptorTable const* (*)(), std::once_flag*, google::protobuf::Metadata const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function __static_initialization_and_destruction_0(int, int): error: undefined reference to 'google::protobuf::internal::AddDescriptorsRunner::AddDescriptorsRunner(google::protobuf::internal::DescriptorTable const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::stringpiece_internal::StringPiece::CheckSize(unsigned long): error: undefined reference to 'google::protobuf::stringpiece_internal::StringPiece::LogFatalSizeTooBig(unsigned long, char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::io::EpsCopyOutputStream::WriteStringMaybeAliased(unsigned int, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, unsigned char*): error: undefined reference to 'google::protobuf::io::EpsCopyOutputStream::WriteStringMaybeAliasedOutline(unsigned int, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, unsigned char*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Arena::~Arena(): error: undefined reference to 'google::protobuf::internal::ThreadSafeArena::~ThreadSafeArena()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Arena::AllocateInternal(unsigned long, unsigned long, void (*)(void*), std::type_info const*): error: undefined reference to 'google::protobuf::Arena::AllocateAlignedWithCleanup(unsigned long, std::type_info const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Arena::AllocateInternal(unsigned long, unsigned long, void (*)(void*), std::type_info const*): error: undefined reference to 'google::protobuf::Arena::AllocateAlignedWithCleanup(unsigned long, std::type_info const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Arena::AllocateAlignedWithHook(unsigned long, unsigned long, std::type_info const*): error: undefined reference to 'google::protobuf::Arena::AllocateAlignedWithHook(unsigned long, std::type_info const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Arena::AllocateAlignedWithHook(unsigned long, unsigned long, std::type_info const*): error: undefined reference to 'google::protobuf::Arena::AllocateAlignedWithHook(unsigned long, std::type_info const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::ArenaStringPtr::DestroyNoArena(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const*): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::DestroyNoArenaSlowPath()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::InternalMetadata::InternalMetadata(google::protobuf::Arena*, bool): error: undefined reference to 'google::protobuf::internal::LogMessage::LogMessage(google::protobuf::LogLevel, char const*, int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::InternalMetadata::InternalMetadata(google::protobuf::Arena*, bool): error: undefined reference to 'google::protobuf::internal::LogMessage::operator<<(char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::InternalMetadata::InternalMetadata(google::protobuf::Arena*, bool): error: undefined reference to 'google::protobuf::internal::LogFinisher::operator=(google::protobuf::internal::LogMessage&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::InternalMetadata::InternalMetadata(google::protobuf::Arena*, bool): error: undefined reference to 'google::protobuf::internal::LogMessage::~LogMessage()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::InternalMetadata::InternalMetadata(google::protobuf::Arena*, bool): error: undefined reference to 'google::protobuf::internal::LogMessage::~LogMessage()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::GetEmptyStringAlreadyInited[abi:cxx11](): error: undefined reference to 'google::protobuf::internal::fixed_address_empty_string[abi:cxx11]'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::MessageLite::MessageLite(): error: undefined reference to 'vtable for google::protobuf::MessageLite'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::MessageLite::MessageLite(google::protobuf::Arena*, bool): error: undefined reference to 'vtable for google::protobuf::MessageLite'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::MessageLite::~MessageLite(): error: undefined reference to 'vtable for google::protobuf::MessageLite'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogMessage::LogMessage(google::protobuf::LogLevel, char const*, int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogMessage::operator<<(char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogFinisher::operator=(google::protobuf::internal::LogMessage&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogMessage::LogMessage(google::protobuf::LogLevel, char const*, int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogMessage::operator<<(char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::LogFinisher::operator=(google::protobuf::internal::LogMessage&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::EpsCopyInputStream::DoneWithCheck(char const**, int): error: undefined reference to 'google::protobuf::internal::EpsCopyInputStream::DoneFallback(int, int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::ReadTag(char const*, unsigned int*, unsigned int): error: undefined reference to 'google::protobuf::internal::ReadTagFallback(char const*, unsigned int)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::internal::VerifyUTF8(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const*, char const*): error: undefined reference to 'google::protobuf::internal::VerifyUTF8(google::protobuf::stringpiece_internal::StringPiece, char const*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::UnknownFieldSet::Clear(): error: undefined reference to 'google::protobuf::UnknownFieldSet::ClearFallback()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Message::Message(): error: undefined reference to 'vtable for google::protobuf::Message'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Message::Message(google::protobuf::Arena*, bool): error: undefined reference to 'vtable for google::protobuf::Message'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function proto::Person::_internal_mutable_name[abi:cxx11](): error: undefined reference to 'google::protobuf::internal::ArenaStringPtr::Mutable[abi:cxx11](google::protobuf::internal::ArenaStringPtr::EmptyDefault, google::protobuf::Arena*)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function google::protobuf::Message::~Message(): error: undefined reference to 'vtable for google::protobuf::Message'
/usr/bin/ld.gold: the vtable symbol may be undefined because the class is missing its key function
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:proto::_Person_default_instance_: error: undefined reference to 'google::protobuf::internal::fixed_address_empty_string[abi:cxx11]'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:proto::Person::_class_data_: error: undefined reference to 'google::protobuf::Message::CopyWithSizeCheck(google::protobuf::Message*, google::protobuf::Message const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:function void google::protobuf::internal::InternalMetadata::DoMergeFrom<google::protobuf::UnknownFieldSet>(google::protobuf::UnknownFieldSet const&): error: undefined reference to 'google::protobuf::UnknownFieldSet::MergeFrom(google::protobuf::UnknownFieldSet const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::GetTypeName[abi:cxx11]() const'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::InitializationErrorString[abi:cxx11]() const'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::CheckTypeAndMergeFrom(google::protobuf::MessageLite const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::CopyFrom(google::protobuf::Message const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::MergeFrom(google::protobuf::Message const&)'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::DiscardUnknownFields()'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:vtable for proto::Person: error: undefined reference to 'google::protobuf::Message::SpaceUsedLong() const'
bazel-out/k8-fastbuild/bin/_objs/person_proto/person.pb.pic.o:person.pb.cc:typeinfo for proto::Person: error: undefined reference to 'typeinfo for google::protobuf::Message'
collect2: error: ld returned 1 exit status
Target //:main failed to build
Use --verbose_failures to see the command lines of failed build steps.
INFO: Elapsed time: 0.176s, Critical Path: 0.06s
INFO: 2 processes: 2 internal.
FAILED: Build did NOT complete successfully

```
