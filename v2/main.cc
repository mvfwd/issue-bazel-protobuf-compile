#include <iostream>
#include "proto-bin/person.pb.h"

int main() {
	proto::Person person;
	person.set_name("John");
	std::cout << "Hello, " << person.name() << std::endl;
	return 0;
}
